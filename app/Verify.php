<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verify extends Model
{
    //
    protected $table = 'verify_user';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
