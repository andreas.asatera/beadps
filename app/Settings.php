<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    public $incrementing = false;
    public $primaryKey = 'config';
}
