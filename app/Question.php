<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $fillable = [
        'soal', 'opsi1', 'opsi2', 'opsi3', 'opsi4', 'opsi5', 'jawaban'
    ];
}
