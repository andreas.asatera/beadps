<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    //
    protected $fillable = [
        'score'
    ];
    public function instansi(){
        return $this->hasOne('App\Instansi', 'kode', 'instansi_id');
    }
}
