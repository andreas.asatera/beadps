<?php

namespace App\Http\Controllers;

use PDF;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    //
    public function download(Request $request){
        $jobs = array_map(null, explode(",", $request->jobtitle), explode(",", $request->jobworktime), explode(",", $request->jobdesc));
        $educations = array_map(null, explode(",", $request->eduname), explode(",", $request->eduqualification));
        $rand = rand().".jpg";
        $canvas = Image::canvas(128, 128);
        $image  = Image::make($request->file('foto')->getRealPath())->resize(128, 128, function($constraint)
        {
            $constraint->aspectRatio();
        });
        $canvas->insert($image, 'center');
        $canvas->save('images/'.$rand);
        $pdf = PDF::loadView('pdf', array_merge($request->except('jobtitle', 'jobworktime', 'jobdesc', 'foto'), array("jobs" => $jobs), array("educations"=> $educations), array("foto" => $rand)));
        return $pdf->download('pdf.pdf');
//        return view('pdf')->with(array_merge($request->except('jobtitle', 'jobworktime', 'jobdesc'), array("jobs" => $jobs), array("foto" => $foto)));
    }
}
