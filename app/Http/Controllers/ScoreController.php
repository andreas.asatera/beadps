<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Score;

class ScoreController extends Controller
{
    //
    public function send(Request $request){
    	$score = new Score();
    	$score->user_id = \Auth::id();
    	$score->score = $request->score;
    	$score->no_peserta = $request->no_peserta;
    	$score->instansi_id = $request->instansi;
    	$score->category_id = $request->category;
    	$score->save();
    	return response()->json([
    		"code" => 1
    	]);
    }
}
