<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Spatie\Browsershot\Browsershot;

class BuatCVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('dashboard.buatcv.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*$pdf = PDF::loadView('pdf', array_merge(array('name' => $request->name), array('name' => $request->name), array('email' => $request->email), array('phone' => $request->nohp), array('profile' => $request->profile)));
        return $pdf->download('pdf.pdf');*/
        /*return view('pdf')->with([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->nohp,
            'profile' => $request->profile
        ]);*/
        $view = \View::make('pdf', array_merge(array('name' => $request->name), array('name' => $request->name), array('email' => $request->email), array('phone' => $request->nohp), array('profile' => $request->profile)))->render();
        return Browsershot::html($view)->save('images/pdf/pdf.png');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
