<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Settings;
use Artisan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (\Schema::hasTable('settings')){
            $active = Settings::where('config', 'active')->first();
            return view('dashboard.settings.index')->with([
                'active' => $active
            ]);
        }
        else return view('dashboard.settings.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        switch ($request->action){
            case 'migrate':
                Artisan::call('migrate');
                break;
            case 'rollback':
                Artisan::call('migrate:rollback');
                break;
        }
        return redirect()->back()->with('success', Artisan::output());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function activate(Request $request){
        $settings = Settings::where('config', 'active')->first();
        $settings->value = $request->active;
        $settings->save();
        if ($request->active == "1"){
            $status = 'success';
            $message = 'Server berhasil dinyalakan';
        }
        elseif ($request->active == "2"){
            $status = 'error';
            $message = 'Server dimatikan';
        }
        return redirect()->back()->with($status, $message);
    }
}
