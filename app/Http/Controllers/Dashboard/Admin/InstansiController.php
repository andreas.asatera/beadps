<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Instansi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $instansis = Instansi::all();
        return view('dashboard.instansi.index')->with([
            'instansis' => $instansis
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dashboard.instansi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $numbers = str_split((string)(int)microtime(true));
        shuffle($numbers);
        $rand = '';
        foreach (array_rand($numbers, 6) as $k) $rand .= $numbers[$k];
        $instansi = new Instansi();
        $instansi->kode = $rand;
        $instansi->nama = $request->nama;
        $instansi->save();
        return redirect()->back()->with('success', "Penambahan instansi berhasil");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
