<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Category;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (!Category::all()->count()) {
            return redirect()->route('dashboard.category.create')->with('error', 'Tambah kategori terlebih dahulu');
        }
        $questions = Question::all();
        return view('dashboard.question.index')->with([
            'questions' => $questions
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (!Category::all()->count()) {
            return redirect()->route('dashboard.category.create')->with('error', 'Tambah kategori terlebih dahulu');
        }
        return view('dashboard.question.create')->with('categories', Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $question = new Question();
        $question->soal = $request->soal;
        $question->opsi1 = $request->opsi1;
        $question->opsi2 = $request->opsi2;
        $question->opsi3 = $request->opsi3;
        $question->opsi4 = $request->opsi4;
        $question->opsi5 = $request->opsi5;
        $question->jawaban = $request->jawaban;
        $question->category_id = $request->category;
        $question->save();
        return redirect()->back()->with('success', 'Berhasil menambahkan soal #'.$question->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
