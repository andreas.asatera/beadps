<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Instansi;
use App\User;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::all();
        return view('dashboard.user.review')->with([
            'users' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $instansi = Instansi::all();
        return view('dashboard.user.create')->with([
            'instansis' => $instansi
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'nama' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6'],
            ],
            [
                'nama.required' => 'Kolom nama harus di isi',
                'email.required' => 'Kolom email harus di isi',
                'email.unique' => 'Email sudah digunakan',
                'password.required' => 'Kolom password harus di isi',
            ]);
        if ($validator->passes()) {
                $user = User::create([
                    'name' => $request->nama,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                ]);
                return redirect()->back()->with('success', 'Pendaftaran user baru: '. $user->name .' berhasil');
        }
        else{
            return redirect()->back()->with('success', implode(", ", $validator->errors()->all()));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (\Auth::id() == $id){
            return redirect()->back()->with('error', 'Tidak bisa menghapus akun sendiri');
        }
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('success', 'Berhasil menghapus user');
    }
}
