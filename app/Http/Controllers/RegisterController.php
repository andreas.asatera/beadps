<?php

namespace App\Http\Controllers;

use App\Instansi;
use App\Mail\VerifyMail;
use App\User;
use App\Verify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ],
            [
                'name.required' => 'Kolom nama harus di isi',
                'email.required' => 'Kolom email harus di isi',
                'email.unique' => 'Email sudah digunakan',
                'password.required' => 'Kolom password harus di isi',
            ]);
        if ($validator->passes()) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            /*$verifyUser = Verify::create([
                'user_id' => $user->id,
                'token' => mt_rand(100000, 999999)
            ]);
            Mail::to($user->email)->send(new VerifyMail($user));*/
            return response()->json([
                "code" => 1,
                "message" => "Pendaftaran berhasil."
            ]);

        } else {
            return response()->json([
                "code" => 2,
                "message" => "Pendaftaran customer gagal: " . implode(", ", $validator->errors()->all())
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $verifyUser = Verify::where([
            "token" => $request->token,
            "user_id" => $id
        ])->first();
        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->hasVerifiedEmail()) {
                $verifyUser->user->markEmailAsVerified();
                $verifyUser->user->save();
            }
            return response()->json([
                "code" => 1,
                "message" => "Verifikasi Email Berhasil"
            ]);
        } else {
            return response()->json([
                "code" => 2,
                "message" => "Verifikasi Email Tidak Berhasil. Mohon ulangi lagi"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
