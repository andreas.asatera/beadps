<?php

namespace App\Http\Controllers;

use App\Instansi;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Validator;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),
            [
                'email' => ['required', 'string'],
                'password' => ['required', 'string', 'min:6'],
            ],
            [
                'name.required' => 'Kolom nama harus di isi',
                'email.required' => 'Kolom email harus di isi',
                'password.required' => 'Kolom password harus di isi',
            ]);
        if ($validator->passes()) {
            $user = User::where("email", $request->email)->first();
            if (isset($user) && Hash::check($request->password, $user->password)) {
                return response()->json([
                    "code" => 1,
                    "message" => "",
                    "id" => $user->id,
                    "name" => $user->name,
                    "email" => $user->email,
                    "verified" => $user->email_verified_at,
                    "token" => $user->createToken(str_random('32'))->accessToken
                ]);

            } else {
                return response()->json([
                    "code" => 2,
                    "message" => "Email atau password salah"
                ]);
            }
        } else {
            /*Gagal*/
            return response()->json([
                "code" => 2,
                "message" => "Login gagal: " . implode(", ", $validator->errors()->all())
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
