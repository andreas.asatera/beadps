<!DOCTYPE html>
<html>
<head>
    <title>Konfirmasi Email Anda</title>
</head>
<body>
<h2>Selamat datang di Bead Psikotes {{$user['name']}}</h2>
<br/>
Silahkan masukkan token di bawah untuk memverifikasi email anda:
<br/>
<b>{{$user->verify->token}}</b>
</body>
</html>