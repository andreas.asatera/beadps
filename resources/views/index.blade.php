<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Template Title -->
    <title>Bantuin Kerja</title>
    {{--    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>--}}
    <link href="{{ asset('blue-app/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('blue-app/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('blue-app/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('blue-app/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('blue-app/style.css') }}" rel="stylesheet">
    <link href="{{ asset('blue-app/css/color/white.css') }}" rel="stylesheet">
    <link href="{{ asset('blue-app/css/responsive.css') }}" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body>
<header id="top">
    <div class="bg-color">
        <div class="top section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7 col-md-7">
                        <div class="content">
                            <h1><strong>BANTUIN</strong> KERJA</h1>
                            <h2>Suatu aplikasi yang membantu calon tenaga kerja agar memahami psikotest kerja melalui uji coba kemampuan dibidang logika/penalaran</h2>
                            <div class="button" id="download-app1">
                                {{--<a href="https://play.google.com/store/apps/details?id=com.tomboltech.beadpsikotes" class="btn btn-default btn-lg custom-btn" target="_blank"><i
                                            class="fa fa-cloud-download"></i>DOWNLOAD</a>--}}
                                <a href="{{ url('/install') }}"><i
                                            class="fa fa-cloud-download"></i>DOWNLOAD</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 col-md-5">
                        <div class="photo-slide">
                            <div id="carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img src="{{ asset('blue-app/images/phone.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<script src="http://code.jquery.com/jquery.min.js"></script>
<script src="{{ asset('blue-app/js/jquery.js') }}"></script>
<script src="{{ asset('blue-app/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('blue-app/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('blue-app/js/jquery.localScroll.min.js') }}"></script>
<script src="{{ asset('blue-app/js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('blue-app/js/jquery.nav.js') }}"></script>
<script src="{{ asset('blue-app/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('blue-app/js/main.js') }}"></script>
<script>
    jQuery(document).ready(function ($) {
        "use strict";
        jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools: false});
    });
</script>
</body>
</html>