<form action="{{ route('question.store') }}" method="POST">
    <label for="soal">Soal #{{ $question->count() + 1 }}:</label><br><textarea name="soal" id="soal" cols="50" rows="3"></textarea><br>
    <label for="opsi1">Opsi 1</label><br><textarea name="opsi1" id="opsi1" cols="50" rows="3"></textarea><br>
    <label for="opsi2">Opsi 2</label><br><textarea name="opsi2" id="opsi2" cols="50" rows="3"></textarea><br>
    <label for="opsi3">Opsi 3</label><br><textarea name="opsi3" id="opsi3" cols="50" rows="3"></textarea><br>
    <label for="opsi4">Opsi 4</label><br><textarea name="opsi4" id="opsi4" cols="50" rows="3"></textarea><br>
    <label for="opsi5">Opsi 5</label><br><textarea name="opsi5" id="opsi5" cols="50" rows="3"></textarea><br>
    <label for="Jawaban">Jawaban</label><br><select name="jawaban" id="Jawaban">
        <option value="1">A</option>
        <option value="2">B</option>
        <option value="3">C</option>
        <option value="4">D</option>
        <option value="5">E</option>
    </select><br>
    <button type="submit">Simpan</button>
</form>