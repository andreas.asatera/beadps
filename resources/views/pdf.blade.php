<!DOCTYPE html>
<html>
<head>
    <title>{{ $name }} - Curriculum Vitae</title>

    <meta name="viewport" content="width=device-width"/>
    <meta name="description" content="The Curriculum Vitae of {{ $name }}."/>
    <meta charset="UTF-8">

    <link type="text/css" rel="stylesheet" href="{{ asset('css/pdf.css') }}">

    <link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700|Lato:400,300' rel='stylesheet' type='text/css'>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
</head>
<body id="top">
<div id="cv" class="instaFade">
    <div class="mainDetails">
        <div id="headshot" class="quickFade">
            {{--            <img src="{{ asset("images/".$foto) }}" alt="{{ $name }}" />--}}
        </div>

        <div id="name">
            <h1 class="quickFade delayTwo">{{ $name }}</h1>
            {{--            <h2 class="quickFade delayThree">Job Title</h2>--}}
        </div>

        <div id="contactDetails" class="quickFade delayFour">
            <ul>
                <li>{{ $email }}</li>
                <li>{{ $phone }}</li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>

    <div id="mainArea" class="quickFade delayFive">
        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Profil Diri</h1>
                </div>

                <div class="sectionContent">
                    <p>{{ $profile }}</p>
                </div>
            </article>
            <div class="clear"></div>
        </section>
        {{--<section>
            <div class="sectionTitle">
                <h1>Pengalaman Kerja</h1>
            </div>

            <div class="sectionContent">
                @foreach($jobs as $job)
                    <article>
                        <h2>{{ $job[0] }}</h2>
                        <p class="subDetails">{{ $job[1] }}</p>
                        <p>{{ $job[2] }}</p>
                    </article>
                @endforeach
            </div>
            <div class="clear"></div>
        </section>


        <section>
            <div class="sectionTitle">
                <h1>Keahlian</h1>
            </div>

            <div class="sectionContent">
                <ul class="keySkills">
                    @php
                        $arr_key = explode(',', $keys);
                        foreach ($arr_key as $key){
                            echo "<li> $key </li>";
                        }
                    @endphp
                </ul>
            </div>
            <div class="clear"></div>
        </section>

        <section>
            <div class="sectionTitle">
                <h1>Pendidikan</h1>
            </div>

            <div class="sectionContent">
                @foreach($educations as $education)
                    <article>
                        <h2>{{ $education[0] }}</h2>
--}}{{--                        <p class="subDetails">Qualification</p>--}}{{--
                        <p>{{ $education[1] }}</p>
                    </article>
                @endforeach
                --}}{{--<article>
                        <h2>College/University</h2>
                        <p class="subDetails">Qualification</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ultricies massa et erat luctus hendrerit. Curabitur non consequat enim.</p>
                    </article>--}}{{--
                --}}{{--@foreach($educations as $education)
                    <article>
                        <h2>{{ $education["name"] }}</h2>
                        <p class="subDetails">{{ $education["type"] }}</p>
                        <p>{{ $education["desc"] }}</p>
                    </article>
                @endforeach--}}{{--

            </div>
            <div class="clear"></div>
        </section>--}}
    </div>
</div>
</body>
</html>