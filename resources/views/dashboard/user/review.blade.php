@extends('layouts.dashboard')
@section('title', 'Review User')
@section('customcss')
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>
@endsection
@section('content')
    <section class="content-header">
        <h1>Review User <a href="{{ route('admin.user.create') }}" class="btn btn-primary">Tambah</a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.user.index') }}"><i class="fa fa-dashboard"></i> User</a></li>
            <li class="active">Review User</li>
        </ol>
        @if (($message = Session::get('success')) || $message = Session::get('error'))
            <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger' }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $message }}
            </div>
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="usersList" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{$user->id}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
                                                        data-target="#modal-delete-{{ $user->id }}"><i class="fa fa-trash" aria-hidden="true"></i>
                                                </button>
                                                <div class="modal fade" id="modal-delete-{{ $user->id }}" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <form action="{{ route('admin.user.destroy', ['id' => $user->id]) }}" method="POST">
                                                                @method('DELETE')
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"
                                                                            aria-label="Close">
                                                                        <span aria-hidden="true">×</span></button>
                                                                    <h4 class="modal-title">Hapus User</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    @csrf
                                                                    Apakah anda yakin untuk menghapus akun ini?
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Batal
                                                                    </button>
                                                                    <button type="submit" class="btn btn-primary">Delete User</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customscript')
    <!-- DataTables -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
    <script>
        $(function () {
            /*$('#usersList').DataTable( {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            })*/
            var table = $('#usersList').DataTable( {
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    }
                ]
            } );

            table.buttons().container()
                .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
        })
    </script>
@endsection