@extends('layouts.dashboard')

@section('title', 'User')

@section('content')
    <section class="content-header">
        <h1>Tambah User</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('user.index') }}"><i class="fa fa-dashboard"></i> User</a></li>
            <li>Tambah User</li>
        </ol>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $message }}
            </div>
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="{{ route('user.store') }}" method="POST">
                                    @csrf
                                    <div class="box-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="inputNama" class="col-sm-2 control-label">Nama User</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" id="inputNama" name="nama"
                                                           placeholder="Nama User">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="email" class="col-sm-2 control-label">Email User</label>
                                                <div class="col-sm-10">
                                                    <input type="email" class="form-control" id="email" name="email"
                                                           placeholder="Email User">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="password" class="col-sm-2 control-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input type="password" class="form-control" id="password" name="password"
                                                           placeholder="Password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-info">Tambah User</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

