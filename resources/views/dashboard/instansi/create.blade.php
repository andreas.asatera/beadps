@extends('layouts.dashboard')
@section('title', 'Tambah Instansi')
@section('content')
    <section class="content-header">
        <h1>Tambah Instansi</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.instansi.index') }}"><i class="fa fa-dashboard"></i> Instansi</a></li>
            <li>Tambah Instansi</li>
        </ol>
        @if (($message = Session::get('success')) || $message = Session::get('error'))
            <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger' }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! $message  !!}
            </div>
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="{{ route('admin.instansi.store') }}" method="POST">
                                    @csrf
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputNama" class="col-sm-2 control-label">Nama Instansi</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputNama" name="nama"
                                                       placeholder="Nama Instansi">
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-info">Tambah Instansi</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection