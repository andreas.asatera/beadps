@extends('layouts.dashboard')

@section('title', 'Soal')

@section('customcss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>
@endsection

@section('content')
    <section class="content-header">
        <h1>Soal <a href="{{ route('admin.question.create') }}" class="btn btn-primary">Tambah</a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.question.index') }}"><i class="fa fa-dashboard"></i> Soal</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="questions" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Soal</th>
                                        <th>Opsi 1</th>
                                        <th>Opsi 2</th>
                                        <th>Opsi 3</th>
                                        <th>Opsi 4</th>
                                        <th>Opsi 5</th>
                                        <th>Jawaban</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($questions as $question)
                                        <tr>
                                            <td>{{$question->id}}</td>
                                            <td>{{$question->soal}}</td>
                                            <td>{{$question->opsi1}}</td>
                                            <td>{{$question->opsi2}}</td>
                                            <td>{{$question->opsi3}}</td>
                                            <td>{{$question->opsi4}}</td>
                                            <td>{{$question->opsi5}}</td>
                                            <td>{{$question->jawaban}}</td>
                                            <td>aksi</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('customscript')
    <!-- DataTables -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
    <script>
        $(function () {
            var table = $('#questions').DataTable( {
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    }
                ]
            } );

            table.buttons().container()
                .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
        })
    </script>
@endsection