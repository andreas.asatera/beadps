@extends('layouts.dashboard')

@section('title', 'Soal')

@section('content')
    <section class="content-header">
        <h1>Soal <a href="{{ route('admin.question.create') }}" class="btn btn-primary">Tambah</a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.question.index') }}"><i class="fa fa-dashboard"></i> Soal</a></li>
        </ol>
    </section>
    <section class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ $message }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="{{ route('admin.question.store') }}" method="POST">
                                    @csrf
                                    <div class="box-body">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="soal">Soal:</label>
                                                <textarea name="soal" id="soal" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="opsi1">Opsi 1:</label>
                                                <textarea name="opsi1" id="opsi1" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="opsi2">Opsi 2:</label>
                                                <textarea name="opsi2" id="opsi2" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="opsi3">Opsi 3:</label>
                                                <textarea name="opsi3" id="opsi3" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="opsi4">Opsi 4:</label>
                                                <textarea name="opsi4" id="opsi4" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="opsi5">Opsi 5:</label>
                                                <textarea name="opsi5" id="opsi5" class="form-control"></textarea>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="jawaban">Jawaban:</label>
                                                <select name="jawaban" id="jawaban" class="form-control">
                                                    <option value="1">A</option>
                                                    <option value="2">B</option>
                                                    <option value="3">C</option>
                                                    <option value="4">D</option>
                                                    <option value="5">E</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="category">Kateogi</label>
                                                <select name="category" id="category" class="form-control">
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-info">Tambah Soal</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

