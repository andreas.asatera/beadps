@extends('layouts.dashboard')

@section('title', 'Kategori')

@section('customcss')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>
@endsection

@section('content')
    <section class="content-header">
        <h1>Kategori <a href="{{ route('admin.category.create') }}" class="btn btn-primary">Tambah</a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.category.index') }}"><i class="fa fa-dashboard"></i> Kategori</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table id="categories" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama Kategori</th>
                                        <th>Aksi</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>{{$category->id}}</td>
                                            <td>{{$category->name}}</td>
                                            <td>aksi</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customscript')
    <!-- DataTables -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
    <script>
        $(function () {
            /*$('#usersList').DataTable( {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            })*/
            var table = $('#categories').DataTable( {
                buttons: [
                    'copy', 'excel', 'pdf'
                ]
            } );

            table.buttons().container()
                .appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );
        })
    </script>
@endsection