@extends('layouts.dashboard')

@section('title', 'Tambah Kategori')

@section('content')
    <section class="content-header">
        <h1>Tambah Kategori</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.category.index') }}"><i class="fa fa-dashboard"></i> Kategori</a></li>
            <li>Tambah Kategori</li>
        </ol>
        @if (($message = Session::get('success')) || $message = Session::get('error'))
            <div class="alert alert-{{ Session::get('success') ? 'success' : 'danger' }} alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!! $message  !!}
            </div>
        @endif
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="{{ route('admin.category.store') }}" method="POST">
                                    @csrf
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="inputNama" class="col-sm-2 control-label">Nama Kategori</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputNama" name="name"
                                                       placeholder="Nama Kategori">
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-info">Tambah Kategori</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

