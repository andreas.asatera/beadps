@extends('layouts.dashboard')
@section('title', 'Dashboard')
@section('content')
    <section class="content-header">
        <h1>Dashboard</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Menu</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <a href="#">
                            <div class="small-box bg-aqua">
                                <div class="text-center">
                                    <i class="fa fa-address-card" style="font-size:70px;margin:10px;"></i>
                                </div>
                                <span class="small-box-footer" style="font-size:16px;">
                          Buat CV <i class="fa fa-arrow-circle-right"></i>
                        </span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if(isset(Auth::user()->roles) && Auth::user()->roles->role == 1)
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-tag"></i> Menu Admin</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{ $users->count() }}</h3>
                                    <p>User</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="{{ route('admin.user.index') }}" class="small-box-footer">More info <i
                                            class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{ $questions->count() }}</h3>
                                    <p>Soal</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </section>
@endsection