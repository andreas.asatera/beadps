@extends('layouts.dashboard')
@section('title', 'Hasil Psikotes')
@section('customcss')
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2-bootstrap.min.css') }}">
@endsection
@section('content')
    <section class="content-header">
        <h1>Hasil Psikotes</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.report.index') }}"><i class="fa fa-dashboard"></i> Hasil Psikotes</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="{{ route('admin.report.store') }}" method="POST">
                                    @csrf
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="instansi" class="col-sm-2 control-label">Nama Instansi</label>
                                            <div class="col-sm-10">
                                                <select name="instansi" id="instansi" class="form-control" style="display: none">
                                                    <option></option>
                                                    @foreach($instansis as $instansi)
                                                        <option value="{{ $instansi->kode }}">{{ $instansi->kode }} - {{ $instansi->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="box-footer text-center">
                                                    <button type="submit" class="btn btn-info">Lihat Laporan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('customscript')
    <!-- DataTables -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.js') }}"></script>
    <script>
        $('#instansi').select2({
            theme: 'bootstrap',
            placeholder: 'PILIH INSTANSI',
            dropdownAutoWidth: true
        })
    </script>
@endsection