@extends('layouts.dashboard')

@section('title', 'Buat CV')

@section('content')
    <section class="content-header">
        <h1>Buat CV</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Buat CV</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="{{ route('buatcv.store') }}"
                                      method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama" class="col-sm-2 control-label">Nama Lengkap</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="nama" name="name"
                                                   placeholder="Nama Lengkap">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Alamat Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="email" name="email"
                                                   placeholder="Alamat Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nohp" class="col-sm-2 control-label">No. Telepon</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="nohp" name="nohp"
                                                   placeholder="No. Telepon">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="profil" class="col-sm-2 control-label">Profil Diri</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="profil" name="profil"
                                                   placeholder="Profil Diri"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="box-footer text-center">
                                            <button type="submit" class="btn btn-info">Buat CV</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

