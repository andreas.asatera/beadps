<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>User Panel | @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('customcss')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="#" class="logo">
            <span class="logo-mini">AP</span>
            <span class="logo-lg">Admin Panel</span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url('dashboard/logout') }}" onclick="event.preventDefault();
							document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ url('dashboard/logout') }}"
                              method="POST">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">NAVIGATION</li>
                <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                    <a href="{{ Request::is('dashboard') ? '#' : route('dashboard.index') }}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ Request::is('dashboard/buatcv*') ? 'active' : '' }}">
                    <a href="{{ Request::is('dashboard/buatcv*') ? '#' : route('buatcv.index') }}">
                        <i class="fa fa-dashboard"></i> <span>Buat CV</span>
                    </a>
                </li>
                @if(isset(Auth::user()->roles) && Auth::user()->roles->role == 1)
                    <li class="header">ADMIN MENU</li>
                    <li class="{{ Request::is('dashboard/admin/user*') ? 'active' : '' }}">
                        <a href="{{ Request::is('dashboard/admin/user*') ? '#' : route('admin.user.index') }}">
                            <i class="fa fa-dashboard"></i> <span>User</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('dashboard/admin/report*') ? 'active' : '' }}">
                        <a href="{{ Request::is('dashboard/admin/report*') ? '#' : route('admin.report.index') }}">
                            <i class="fa fa-dashboard"></i> <span>Hasil Psikotes</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('dashboard/admin/instansi*') ? 'active' : '' }}">
                        <a href="{{ Request::is('dashboard/admin/instansi*') ? '#' : route('admin.instansi.index') }}">
                            <i class="fa fa-dashboard"></i> <span>Instansi</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('dashboard/admin/category*') ? 'active' : '' }}">
                        <a href="{{ Request::is('dashboard/admin/category*') ? '#' : route('admin.category.index') }}">
                            <i class="fa fa-dashboard"></i> <span>Kategori</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('dashboard/admin/question*') ? 'active' : '' }}">
                        <a href="{{ Request::is('dashboard/admin/question*') ? '#' : route('admin.question.index') }}">
                            <i class="fa fa-dashboard"></i> <span>Soal</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('dashboard/admin/settings&*') ? 'active' : '' }}">
                        <a href="{{ Request::is('dashboard/admin/settings*') ? '#' : route('admin.settings.index') }}">
                            <i class="fa fa-dashboard"></i> <span>Settings</span>
                        </a>
                    </li>
                @endif
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        @yield('content')
    </div>
</div>
<script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/chart.js/Chart.js') }}"></script>
@yield('customscript')
</body>
</html>
