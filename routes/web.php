<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('/install', function () {
    return response()->download(public_path('download/BeadPsikotes.apk'));
})->name('install');


// API
Route::post('/api/register', 'RegisterController@store');
Route::post('/api/login', 'LoginController@store');
Route::group(['middleware' => 'auth:api', 'prefix' => 'api'], function () {
    Route::post('/user', 'UserController@show');
    Route::apiResource('/instansi', 'InstansiController');
    Route::resource('/question', 'QuestionController');
    Route::post('score/send', 'ScoreController@send');
    Route::post('pdf', 'PDFController@download');
});

// Web
Route::get('login', 'Dashboard\DashboardController@login')->name('login');
Route::post('login', 'Dashboard\DashboardController@store')->name('login-post');
Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::post('logout', 'Dashboard\DashboardController@destroy')->name('logout');
        Route::resource('buatcv', 'Dashboard\BuatCVController');
        Route::group(['middleware' => ['isAdmin'], 'prefix' => 'admin'], function () {
            Route::resource('user', 'Dashboard\Admin\UserController', ['as' => 'admin']);
            Route::post('settings/activate', 'Dashboard\Admin\SettingsController@activate', ['as' => 'admin'])->name('activate');
            Route::resource('settings', 'Dashboard\Admin\SettingsController', ['as' => 'admin']);
            Route::resource('instansi', 'Dashboard\Admin\InstansiController', ['as' => 'admin']);
            Route::resource('report', 'Dashboard\Admin\ReportController', ['as' => 'admin']);
            Route::resource('category', 'Dashboard\Admin\CategoryController', ['as' => 'admin']);
            Route::resource('question', 'Dashboard\Admin\QuestionController', ['as' => 'admin']);
        });
    });
    Route::resource('dashboard', 'Dashboard\DashboardController');
});